# CIGeoE Pacman

When acquiring features in the field, and after doing some updates and validations, there may exist areas that overlap. To avoid manual editing and shorten the time doing it, this plugin defines the overlapped area as belonging to the first polygon and removing it from the second.

# Installation

For QGIS 3:

There are two methods to install the plugin:

- Method 1:

  1- Copy the folder “cigeoe_pacman” to folder:

  ```console
  [drive]:\Users\[user]\AppData\Roaming\QGIS\QGIS3\profiles\default\python\plugins
  ```

  2 - Start QGis and go to menu "Plugins" and select "Manage and Install Plugins"

  3 - Select “CIGeoE Pacman”

  4 - After confirm the operation the plugin will be available in toolbar

- Method 2:

  1 - Compress the folder “cigeoe_pacman” (.zip).

  2 - Start QGis and go to "Plugins" and select “Manage and Install Plugins”

  3 - Select “Install from Zip” (choose .zip generated in point 1) and then select "Install Plugin"

# Usage

1 - Activate the "Select features" tool, then select two features that intersect each other. The first feature will stay equal, but the second one will be without the intersected area.

![ALT](/images/image05.png)

![ALT](/images/image06.png)

2 - Click on the plugin icon

![ALT](/images/image07.png)


# Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

# License

[AGPL 3.0](https://www.gnu.org/licenses/agpl-3.0.en.html)
