# -*- coding: utf-8 -*-
"""
/***************************************************************************
 CIGeoEPacman
                                 A QGIS plugin
 Resolve the intersection of 2 polygons by removing the overlapped area in the second polygon.
 Generated by Plugin Builder: http://g-sherman.github.io/Qgis-Plugin-Builder/
                              -------------------
        begin                : 2019-01-31
        git sha              : $Format:%H$
        copyright            : (C) 2019 by Centro de Informação Geoespacial do Exército 
        email                : igeoe@igeoe.pt
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""
from PyQt5.QtCore import QSettings, QTranslator, qVersion, QCoreApplication, Qt
from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QAction, QToolBar, QMessageBox

from qgis.gui import QgsMessageBar
from qgis.core import *


# Initialize Qt resources from file resources.py
from .resources import *

# Import the code for the DockWidget
from .CIGeoE_Pacman_dockwidget import CIGeoEPacmanDockWidget
import os.path


class CIGeoEPacman:
    """QGIS Plugin Implementation."""

    def __init__(self, iface):
        """Constructor.

        :param iface: An interface instance that will be passed to this class
            which provides the hook by which you can manipulate the QGIS
            application at run time.
        :type iface: QgsInterface
        """
        # Save reference to the QGIS interface
        self.iface = iface

        # initialize plugin directory
        self.plugin_dir = os.path.dirname(__file__)

        # initialize locale
        locale = QSettings().value('locale/userLocale')[0:2]
        locale_path = os.path.join(
            self.plugin_dir,
            'i18n',
            'CIGeoEPacman_{}.qm'.format(locale))

        if os.path.exists(locale_path):
            self.translator = QTranslator()
            self.translator.load(locale_path)

            if qVersion() > '4.3.3':
                QCoreApplication.installTranslator(self.translator)

        # Declare instance attributes
        self.actions = []
        self.menu = self.tr(u'&CIGeoE Pacman')

        # Check for CIGeoE toolbar. If exists, add button there; if not exists, create one
        cigeoeToolBarExists = False
        for x in iface.mainWindow().findChildren(QToolBar): 
            if x.windowTitle() == 'CIGeoE':
                self.toolbar = x
                cigeoeToolBarExists = True
        if cigeoeToolBarExists==False:
            #self.toolbar = self.iface.addToolBar(u'CIGeoE')
            self.toolbar.setObjectName(u'CIGeoE Pacman')


        #print "** INITIALIZING CIGeoEPacman"

        self.pluginIsActive = False
        self.dockwidget = None

        self.selectedFeaturesMap={}
        self.iface.mapCanvas().selectionChanged.connect(self.mapSelectionChanged)


    def mapSelectionChanged(self, layer):
        if("0" not in self.selectedFeaturesMap):
            self.selectedFeaturesMap["0"]={"layer":layer, "features":layer.selectedFeatures()}
        elif("1" not in self.selectedFeaturesMap):
            self.selectedFeaturesMap["1"]={"layer":layer, "features":layer.selectedFeatures()}
        else:
            self.selectedFeaturesMap["0"]=self.selectedFeaturesMap["1"]
            self.selectedFeaturesMap["1"]={"layer":layer, "features":layer.selectedFeatures()}
        return


    # noinspection PyMethodMayBeStatic
    def tr(self, message):
        """Get the translation for a string using Qt translation API.

        We implement this ourselves since we do not inherit QObject.

        :param message: String for translation.
        :type message: str, QString

        :returns: Translated version of message.
        :rtype: QString
        """
        # noinspection PyTypeChecker,PyArgumentList,PyCallByClass
        return QCoreApplication.translate('CIGeoEPacman', message)


    def add_action(
        self,
        icon_path,
        text,
        callback,
        enabled_flag=True,
        add_to_menu=True,
        add_to_toolbar=True,
        status_tip=None,
        whats_this=None,
        parent=None):
        """Add a toolbar icon to the toolbar.

        :param icon_path: Path to the icon for this action. Can be a resource
            path (e.g. ':/plugins/foo/bar.png') or a normal file system path.
        :type icon_path: str

        :param text: Text that should be shown in menu items for this action.
        :type text: str

        :param callback: Function to be called when the action is triggered.
        :type callback: function

        :param enabled_flag: A flag indicating if the action should be enabled
            by default. Defaults to True.
        :type enabled_flag: bool

        :param add_to_menu: Flag indicating whether the action should also
            be added to the menu. Defaults to True.
        :type add_to_menu: bool

        :param add_to_toolbar: Flag indicating whether the action should also
            be added to the toolbar. Defaults to True.
        :type add_to_toolbar: bool

        :param status_tip: Optional text to show in a popup when mouse pointer
            hovers over the action.
        :type status_tip: str

        :param parent: Parent widget for the new action. Defaults None.
        :type parent: QWidget

        :param whats_this: Optional text to show in the status bar when the
            mouse pointer hovers over the action.

        :returns: The action that was created. Note that the action is also
            added to self.actions list.
        :rtype: QAction
        """

        icon = QIcon(icon_path)
        action = QAction(icon, text, parent)
        action.triggered.connect(callback)
        action.setEnabled(enabled_flag)

        if status_tip is not None:
            action.setStatusTip(status_tip)

        if whats_this is not None:
            action.setWhatsThis(whats_this)

        if add_to_toolbar:
            self.toolbar.addAction(action)

        if add_to_menu:
            self.iface.addPluginToMenu(
                self.menu,
                action)

        self.actions.append(action)

        return action


    def initGui(self):
        """Create the menu entries and toolbar icons inside the QGIS GUI."""

        icon_path = ':/plugins/CIGeoE_Pacman/icon.png'
        self.add_action(
            icon_path,
            text=self.tr(u'CIGeoE Pacman: Resolve the intersection of 2 polygons by removing the overlapped area in the second polygon.'),
            callback=self.run,
            parent=self.iface.mainWindow())

    #--------------------------------------------------------------------------

    def onClosePlugin(self):
        """Cleanup necessary items here when plugin dockwidget is closed"""

        #print "** CLOSING CIGeoEPacman"

        # disconnects
        self.dockwidget.closingPlugin.disconnect(self.onClosePlugin)

        # remove this statement if dockwidget is to remain
        # for reuse if plugin is reopened
        # Commented next statement since it causes QGIS crashe
        # when closing the docked window:
        # self.dockwidget = None

        self.pluginIsActive = False


    def unload(self):
        """Removes the plugin menu item and icon from QGIS GUI."""

        #print "** UNLOAD CIGeoEPacman"

        for action in self.actions:
            self.iface.removePluginMenu(
                self.tr(u'&CIGeoE Pacman'),
                action)
            self.iface.removeToolBarIcon(action)
        # remove the toolbar
        del self.toolbar

    #--------------------------------------------------------------------------

    def run(self):
        """Run method that loads and starts the plugin"""
        self.eatAreaBtnHandler()

    def eatAreaBtnHandler(self):
        selected_features=[]
        activeLayer = self.iface.activeLayer()

        if not activeLayer:        
            QMessageBox.information(self.iface.mainWindow(), "Error", 'Layer is not loaded!')            
            return 



        if(activeLayer is not None):
            #check if there are other layers, than the active one, with selected features. If yes, it will be
            #the first feature
            #layers = self.iface.legendInterface().layers()                     # pyqgis2
            layers = QgsProject.instance().layerTreeRoot().layerOrder()   
            for layer in layers:
                if(layer.type() == QgsMapLayer.VectorLayer):
                    for f in layer.selectedFeatures():
                        selected_features.append( f )
                        
            #wont advance if there's more or less than 2 selected features
            if(len(selected_features)!=2):
                QMessageBox.information(self.iface.mainWindow(), "Error", 'To resolve intersection, you must exactly select 2 polygons that overlap.')
                return

            #check which feature is the first or the second
            feat1full=None
            feat2full=None
            #if layers are different, first feature is of map "0" and second is of map "1"
            if self.selectedFeaturesMap["0"]["layer"] != self.selectedFeaturesMap["1"]["layer"]:
                feat1full={"layer" : self.selectedFeaturesMap["0"]["layer"], "feature" : self.selectedFeaturesMap["0"]["features"][0]}
                feat2full={"layer" : self.selectedFeaturesMap["1"]["layer"], "feature" : self.selectedFeaturesMap["1"]["features"][0]}
            else:
                #if is the same layer, the list that has only one element is the first
                if len(self.selectedFeaturesMap["0"]["features"])==1:
                    feat1full={"layer":self.selectedFeaturesMap["0"]["layer"], "feature":self.selectedFeaturesMap["0"]["features"][0]}
                    for f in self.selectedFeaturesMap["1"]["features"]:
                        if f.id()!=feat1full["feature"].id():
                            feat2full={"layer":self.selectedFeaturesMap["1"]["layer"], "feature":f}
                else:
                    feat1full={"layer":self.selectedFeaturesMap["1"]["layer"], "feature":self.selectedFeaturesMap["1"]["features"][0]}
                    for f in self.selectedFeaturesMap["0"]["features"]:
                        if f.id()!=feat1full["feature"].id():
                            feat2full={"layer":self.selectedFeaturesMap["0"]["layer"], "feature":f}
            feat1=feat1full["feature"]
            feat2=feat2full["feature"]

            #wont advance if any of the two features is not a polygon
            #if feat1.geometry().type()!=QGis.Polygon or feat2.geometry().type()!=QGis.Polygon:             # pyqgis2
            if feat1.geometry().type()!=QgsWkbTypes.PolygonGeometry or feat2.geometry().type()!=QgsWkbTypes.PolygonGeometry:
                QMessageBox.information(self.iface.mainWindow(), "Error", 'To resolve intersection, both features must be of Polygon type.')
                return

            #wont advance if both features do not intersect each other
            if (feat1.geometry().intersects(feat2.geometry())==False):
                QMessageBox.information(self.iface.mainWindow(), "Error", "Selected features do not intersect each other.")
                return
            
            feat2full["layer"].startEditing()


            newFeature2=QgsFeature()
            newFeature2.setGeometry( feat2.geometry().difference( feat1.geometry() ) )
            newFeature2.setFields(feat2.fields())
            for field in feat2.fields():
                newFeature2.setAttribute(field.name(), feat2[field.name()])

            #check if newFeature2 become multipolygon after execution and, if yes, transform in multiple polygons
            remove_list=[]
            if newFeature2.geometry().isMultipart():
                new_features=[]
                temp_feature=QgsFeature(newFeature2)
                # create a new feature using the geometry of each part
                for part in newFeature2.geometry().asGeometryCollection ():
                    temp_feature.setGeometry(part)
                    newestFeature=QgsFeature(temp_feature)
                    #set fields to newestFeature
                    for field in feat2.fields():
                        newestFeature.setAttribute(field.name(), feat2[field.name()])
                    #fill new_features array
                    new_features.append(newestFeature)
                # add new features to layer
                #feat2full["layer"].addFeatures(new_features, False)     # pyqgis2
                feat2full["layer"].addFeatures(new_features)
                #does not add newFeature2 to layer!
            else:
                #feat2full["layer"].addFeature(newFeature2, True)         # pyqgis2
                feat2full["layer"].addFeature(newFeature2)
 				
            feat2full["layer"].deleteFeature(feat2.id())

            feat2full["layer"].triggerRepaint()

            del self.selectedFeaturesMap["0"]
            del self.selectedFeaturesMap["1"]   


            #QgsMessageLog.logMessage("Intersection resolved and now belongs to first feature.", 'CIGeoE Pacman')
            self.iface.messageBar().pushMessage("CIGeoE Pacman", "Intersection resolved and now belongs to first feature." , level=Qgis.Info, duration=2)



        